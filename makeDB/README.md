# 数据库生成工具  
## 介绍  
从原始的fasta文件生成聚类需要的数据库。  
支持多线程，如果用ssd，最好能有8线程。  
### 编译  
g++ -fopenmp src/makeDB.cpp -o makeDB  
可以参考Makefile  
### 运行  
./makeDB -f ../data/Homo_sapiens.GRCh37.70.pep.all.fa -o ../data.bin -t 1  
可以参考Makefile  
### 参数说明  
-f 输入的fasta文件  
-b 生成的bin文件  
-t 输入数据的类型，0是基因序列，1是蛋白序列  
### 输出说明  
data.bin 在输出目录生成的数据库文件，包含聚类需要的数据结构。  

