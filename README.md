# nGIA序列聚类(去冗余)工具套件  
## 介绍  
采用贪婪增量算法的序列聚类(去冗余)工具套件，当前唯一可以进行序列精确聚类的工具，也是速度最快的(用多节点)。  
支持单节点,多节点,CUDA平台,OneAPI平台。  
### 应用范围  
-> 序列个数 1-2,147,483,647  
-> 序列长度 1-65536  
-> 相似度 0.1-1.0  
以上范围都可以通过更改数据结构int为long，轻松扩展，但不推荐。因为数据集太大一次要跑几天，没人会等，没意义。  
### 使用介绍  
首先用makeDB生成数据库。  
选择一个版本(多/单节点)的聚类工具进行聚类。  
具体可以参考每个文件夹中的readme
#### 用单节点，cuda版本进行基因序列聚类  
创建工作目录  
mkdir temp  
解压数据  
gzip -c data/current_NCBI_gg16S_unaligned.fasta.gz > temp/gene.fasta  
编译makeDB工具  
g++ -fopenmp makeDB/src/makeDB.cpp -o temp/makeDB  
生成数据库  
temp/makeDB -f temp/gene.fasta -b temp/data.bin -t 0  
编译单节点，CUDA版本聚类工具  
nvcc CUDA/signalNode/src/cluster.cpp CUDA/signalNode/src/func.cu -o temp/cluster  
聚类  
temp/cluster -f temp/gene.fasta -b temp/data.bin -o temp -s 0.95  
### 运行需求  
需要Nvidia显卡。  
多节点版本需要MPI支持。  
### 瓶颈分析  
序列长度越长，计算量越大，也就越慢。  
生成数据库阶段瓶颈是硬盘小文件IO。  
多节点版本，数据小，节点多，会使得通信占比过高，平均一张卡至少要给1G数据。  
# nGIA序列聚类(去冗余)工具套件CUDA版本  
## MultiNode  
多节点版本，支持MPI，
### 多节点版本编译  
nvcc -I"MPI include" -L"MPI lib" -lmpi src/cluster.cpp src/func.cpp -o cluster  
可以参考对应的Makefile  
### 多节点版本运行  
mpirun -n <threadCount> ./cluster -f <fastaFile> -b <binFile> -o <outputDIr> -s <similarity>  
可以参考对应的Makefile  
## SignalNode  
单节点版本，由多节点版本去掉MPI通讯后简化而来。  
单节点性能与多节点跑1个MPI进程性能相同，但优化掉MPI数据结构也不会更快。  
### 单节点版本编译  
nvcc src/cluster.cpp src/func.cpp -o cluster  
可以参考对应的Makefile  
### 单节点版本运行  
./cluster -f <fastaFile> -b <binFile> -o <outputDIr> -s <similarity>  
可以参考对应的Makefile  
## 参数说明  
-f fasta文件  
-b bin文件，makeDB工具生成  
-s 相似度，范围是0.1-1.0  
-p 是否启用精确模式，精确模式可以得到金标准，但是速度更慢  
## 输出说明  
represent.fasta 数据集去冗余之后的结果  
cluster.txt 序列剧烈的结果，无缩进的行是代表序列名，有缩进的是类内序列名  
